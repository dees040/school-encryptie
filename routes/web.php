<?php

Route::get('/', function () {
    return view('posts.index');
});

Route::resource('posts', 'PostController');