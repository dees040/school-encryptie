<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Encryption</title>
</head>
<body>
<form action="{{ route('posts.store') }}">
    <div>
        Name: <input type="text" name="name" value="{{ old('name') }}">
    </div>
    <div>
        Wachtwoord: <input type="password" name="password">
    </div>
    <div>
        <p>
            Laat de tekst leeg als je een tekst wil ophalen. Als je een tekst invult wordt deze opgeslagen in de
            database via encryption.
        </p>
        Tekst: <textarea type="text" name="body">{{ old('name') }}</textarea>
    </div>
</form>
</body>
</html>